
Ext.define('Pertemuan9.view.main.FormPanel', {
    extend: 'Ext.form.Panel',


    shadow: true,
    xtype:'editform',
    id: 'editform',
    items: [
    {

        xtype: 'textfield',
        name: 'name',
        id:'myname',
        label: 'Name',
        placeHolder: 'Your Name',
        autoCapitalize: true,
        required: true,
        clearIcon: true
    },
    {
        xtype: 'emailfield',
        name: 'email',
        id:'myemail',
        label: 'Email',
        placeHolder: 'me@sencha.com',
        clearIcon: true
    },
    {
        xtype: 'textfield',
        name: 'phone',
        id:'myphone',
        label: 'Phone',
        placeHolder: '+62',
        clearIcon: true
    },
    {
      xtype: 'button',
      ui:'action',
      text: 'Simpan Perubahan',
      handler :'onSimpanPerubahan'
  },
  {
    xtype: 'button',
    ui:'action',
    text: 'Tambah Data',
    handler :'onTambahData'
}
]
});
