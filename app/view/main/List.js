/**
 * This view is an example list of people.
 */
 Ext.define('Pertemuan9.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
    'Pertemuan9.store.Personnel'
    ],

    title: 'DATA ANGGOTA SENCHA',
    requires: [
        'Ext.grid.plugin.Editable'
      ],
      plugins: [{
        type: 'grideditable'
    }],

    /*store: {
        type: 'personnel'
    },*/

    bind: '{personnel}',

    viewModel: {
        stores: {
            personnel: {
                type: 'personnel'
            }
        }
    },

    columns: [
    { text: 'Name',  dataIndex: 'name', width: 220, editable: true },
    { text: 'Email', dataIndex: 'email', width: 230, editable: true },
    { text: 'Phone', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
